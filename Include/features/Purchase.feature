Feature: Purchase Feature

  Scenario: User can add a Midtrans Pillow to shopping cart
  	Given User open Midtrans website via Chrome or Firefox
  	And User navigate to Midtrans Sample Store screen
  	When User click on 'BUY NOW' button
  	Then Shopping cart is displayed 
  	
	Scenario: Check that customer can checkout the pillow
		Given User can see the product in shopping cart
		And User fill the Customer Detail form
		When User click on 'Checkout' button
		Then User can see the Order Summary screen
		And User click on 'Continue' button to continue the process
		
	Scenario: User choose Credit Card as payment method
		Given User navigate to Select Payment screen
		When User click on 'Credit Card' button list
		Then Credit card screen is displayed
		
	Scenario Outline: User can pay the product with credit card
		Given User navigate to Credit card detail screen
		When total amount to be paid is shown
		And User input <creditcardnumber>
		And User input <expiredate> of credit card
		And User input <CCV> of credit card
		And Notification of unable change payment method is displayed
		Then Click 'PAY NOW' button
		
		Examples:
			| creditcardnumber |  expiredate | CCV |
			| 4811 1111 1111 1114 | 02/20	| 123 |
			| 4911 1111 1111 1114 | 02/20	| 123 |
		
	Scenario Outline: User verify the transaction
		Given Issuing Bank screen is displayed
		When Transaction time is shown
		Then User input the <password>
		And Click 'OK' button 
		And User get information about success transaction
		
		Examples:
			| password |
			| 4tAN/DuJV7Y= | 