import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class PurchaseSteps {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User open Midtrans website via Chrome or Firefox")
	def openMidtransWebsite(){
		println ("\n I'm insed openMidtransWebsite")
		WebUI.openBrowser('')
		
	}

	@And("User navigate to Midtrans Sample Store screen")
	def navigateToSampleStoreScreen(){
		println ("\n I'm inside navigateToSampleStoreScreen")
		WebUI.navigateToUrl('https://demo.midtrans.com/')
		
	}
	
	@When("User click on 'BUY NOW' button")
	def buyNowButton(){
		println ("\n I'm inside buyNowButton")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_BUY NOW'))
		
	}
	
	@Then("Shopping cart is displayed")
	def shoppingCartScreenIsDisplayed(){
		println ("\n I'm inside shoppingCartScreenIsDisplayed")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/span_Shopping Cart'), 0)
		
	}
	
	@Given("User can see the product in shopping cart")
	def seeProductInShoppingCart(){
		println ("\n I'm inside seeProductInShoppingCart")
		WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Sample Store/td_Midtrans Pillow'))
		
	}
	
	@And("User fill the Customer Detail form")
	def fillCustomerDetailForm(){
		println ("\n I'm inside fillCustomerDetailForm")
		WebUI.click(findTestObject('Page_Sample Store/input'))
		
	}
	
	@When("User click on 'Checkout' button")
	def clickCheckoutButton(){
		println ("\n I'm inside clickCheckoutButton")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_CHECKOUT'))
		
	}
	
	@Then("User can see the Order Summary screen")
	def orderSummaryScreen(){
		println ("\n I'm inside orderSummaryScreen")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/p_Order Summary'), 0)
		
	}
	
	@And("User click on 'Continue' button to continue the process")
	def clickContinueButton(){
		println ("\n I'm inside clickContinueButton")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue'))
		
	}
	
	@Given("User navigate to Select Payment screen")
	def navigateToPaymentScreen(){
		println ("\n I'm inside navigateToPaymentScreen")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/p_Select Payment'), 0)
		
	}
	
	@When("User click on 'Credit Card' button list")
	def clickCreditCardButtonList(){
		println ("\n I'm inside clickCreditCardButtonList")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Credit CardPay with Visa MasterCard JCB o_fd4abe'))
		
	}
	
	@Then("Credit card screen is displayed")
	def creditCardScreenIsDisplayed(){
		println ("\n I'm inside creditCardScreenIsDisplayed")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/p_Credit Card'), 0)
		
	}
	
	
	
	@Given("User navigate to Credit card detail screen")
	def navigateToCreditCardDetailScreen(){
		println ("\n I'm inside navigateToCreditCardDetailScreen")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/p_Credit Card'), 0)
		
	}
	
	@When("total amount to be paid is shown")
	def totalAmountIsShown(){
		println ("\n I'm inside totalAmountIsShown")
		WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Sample Store/span_19000'))
		
	}
	
	@And("User input (.*)")
	def inputCreditCardNumbar(String creditcardnumber){
		println ("\n I'm inside inputCreditCardNumbar")
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582072435_cardnumber'), creditcardnumber)
		
	}
	
	@And("User input (.*) of credit card")
	def inputExpireDate(String expiredate){
		println ("\n I'm inside inputExpireDate")
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input (1)'), expiredate)
		
	}
	
	@And("User input (.*) of credit card")
	def inputCCV(String CCV){
		println ("\n I'm inside inputCCV")
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1'), CCV)
		
	}
	
	@And("Notification of unable change payment method is displayed")
	def notificationUnableChangePaymentMethodeDisplayed(){
		println ("\n I'm inside notificationUnableChangePaymentMethodeDisplayed")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/span_IMPORTANT Once you confirm payment you_d7af11'),
			0)
	}
	
	@Then("Click 'PAY NOW' button")
	def clickPayNowButton(){
		println ("\n I'm inside clickPayNowButton")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now'))
		
	}
	
	@Given("Issuing Bank screen is displayed")
	def issuingBankScreen(){
		println ("\n I'm inside issuingBankScreen")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/h1_Issuing Bank'), 0)
		
	}
	
	@When("Transaction time is shown")
	def transactionTimeShown(){
		println ("\n I'm inside transactionTimeShown")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/span_Transaction time left -  04  39'), 0)
		
	}
	
	@Then("User input the (.*)")
	def userInputPassword(String password){
		println ("\n I'm inside userInputPassword")
		println ("Bank's OTP : "+password)
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes'), password)
		
	}
	
	@And("Click 'OK' button")
	def clickOkButton(){
		println ("\n I'm inside clickOkButton")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK'))
	}
	
	@And("User get information about success transaction")
	def getInformationSuccessTransaction(){
		println ("\n I'm inside getInformationSuccessTransaction")
		WebUI.waitForPageLoad(6)
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Sample Store/div_Thank you for your purchaseGet a nice sleep'),
			0)
		
	}
}

